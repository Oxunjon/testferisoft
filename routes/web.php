<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home',  [HomeController::class, 'index'])->name('home');
Route::get('/contact',  [HomeController::class, 'contact'])->name('contact');
Route::get('/admin',[AdminController::class, 'index'])->name('admin');
Route::get('/admin/form',[AdminController::class, 'store'])->name('admin.store');