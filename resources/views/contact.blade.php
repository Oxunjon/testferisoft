@extends('app')

@section('contents')
<div class="col-md-6">
<form action="{{url('admin/form')}}" method="POST">
    @csrf
    <div class="form-group"> 
        <div class="form-group">
            <label for="exampleInputName">Name</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="name">
          </div>
          <label for="exampleFormControlTextarea1">Message </label>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
      </div>
    <button type="submit" class="btn btn-primary" >submit</button>
</div>
</form>
    </div>
    <div class="col-md-6" style="padding:50px">

    </div>
@endsection

@push('scripts')
 
    <script>

    </script>
@endpush